package ru.ptv.portfolio.dto;

import lombok.Builder;
import lombok.Data;
import ru.ptv.portfolio.models.Account;
import ru.ptv.portfolio.models.Role;

/**
 * 27.06.2021 - 0:06
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@Builder
public class AccountDto {
    private String firstName;
    private String lastName;
    private Role role;

    public static AccountDto from(Account account) {
        return AccountDto.builder()
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .role(account.getRole())
                .build();
    }
}
