package ru.ptv.portfolio.forms;

import lombok.Data;
import ru.ptv.portfolio.models.Role;
import ru.ptv.portfolio.models.State;

/**
 * 10.07.2021 - 12:20
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
public class PageAdminAccountForm {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private Role role;
    private State state;
}
