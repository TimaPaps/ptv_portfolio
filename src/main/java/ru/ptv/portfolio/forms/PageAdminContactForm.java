package ru.ptv.portfolio.forms;

import lombok.Data;

/**
 * 08.07.2021 - 18:38
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
public class PageAdminContactForm {
    private String image;
    private String link;
    private String alt;
}
