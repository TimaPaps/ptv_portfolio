package ru.ptv.portfolio.forms;

import lombok.Data;

/**
 * 22.06.2021 - 22:50
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
public class PostForm {
    private String title;
    private String anons;
    private String fullText;
    private Integer views;
}
