package ru.ptv.portfolio.forms;

import lombok.Data;

/**
 * 24.06.2021 - 17:52
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
public class SignInForm {
    private String email;
    private String password;
}
