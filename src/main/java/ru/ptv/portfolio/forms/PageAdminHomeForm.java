package ru.ptv.portfolio.forms;

import lombok.Data;

/**
 * 06.07.2021 - 13:36
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
public class PageAdminHomeForm {
    private String text_header;
    private String text_intro;
    private String text_desc;
}
