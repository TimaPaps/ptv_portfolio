package ru.ptv.portfolio.forms;

import lombok.Data;

/**
 * 24.06.2021 - 17:24
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
public class SignUpForm {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String password;
}
