package ru.ptv.portfolio.consts;

/**
 * 30.06.2021 - 13:06
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Constants {
    public static final Integer VIEWS_DEFAULT = 0;
    public static final Byte ONLY_ONE_ENTRY = 1;
}
