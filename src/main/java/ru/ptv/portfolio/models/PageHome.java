package ru.ptv.portfolio.models;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

/**
 * 06.07.2021 - 13:15
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageHome {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Byte id;

    @NotNull
    @Column(columnDefinition = "varchar(50) default 'Заголовок страницы - h1'")
    private String text_header;
    @NotNull
    @Column(columnDefinition = "varchar(1000) default 'Обязательная вступительная часть страницы'")
    private String text_intro;
    @Column(length = 2000)
    private String text_desc;
}
