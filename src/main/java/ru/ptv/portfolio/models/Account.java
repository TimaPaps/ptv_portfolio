package ru.ptv.portfolio.models;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * 24.06.2021 - 17:22
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Entity
@Data
@ToString(exclude = "posts")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 50)
    private String firstName;
    @NotNull
    @Column(length = 50)
    private String lastName;
    @NotNull
    @Column(length = 50, unique = true)
    private String email;
    @Column(length = 20, unique = true)
    private String phone;
    @Column(length = 100)
    private String hashPassword;
    private Timestamp dateRegistration;

    @Enumerated(value = EnumType.STRING)
    private Role role;
    @Enumerated(value = EnumType.STRING)
    private State state;

    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    private List<Post> posts;
}
