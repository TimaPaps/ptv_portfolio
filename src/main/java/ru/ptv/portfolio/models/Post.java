package ru.ptv.portfolio.models;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

/**
 * 21.06.2021 - 21:08
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Entity
@Data
@ToString(exclude = "owner")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "varchar(255) default 'Заголовок статьи'")
    private String title;
    @Column(columnDefinition = "varchar(255) default 'Анонс статьи'")
    private String anons;
    @Column(columnDefinition = "varchar(3000) default 'Текст Статьи'")
    private String fullText;
    @NotNull
    @Column(columnDefinition = "integer default 0")
    private Integer views;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Account owner;
}
