package ru.ptv.portfolio.models;

/**
 * 24.06.2021 - 17:23
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public enum Role {
    ADMIN,
    USER
}
