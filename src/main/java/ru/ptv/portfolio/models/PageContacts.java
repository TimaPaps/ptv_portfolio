package ru.ptv.portfolio.models;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;

/**
 * 07.07.2021 - 20:14
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageContacts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 100)
    private String image;
    @NotNull
    @Column(length = 1000)
    private String link;
    @Column(length = 50)
    private String alt;
}
