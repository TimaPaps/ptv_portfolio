package ru.ptv.portfolio.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import ru.ptv.portfolio.forms.PostForm;
import ru.ptv.portfolio.models.Account;
import ru.ptv.portfolio.models.Post;
import ru.ptv.portfolio.repositories.PostRepository;

import java.util.ArrayList;
import java.util.Optional;

import static ru.ptv.portfolio.consts.Constants.VIEWS_DEFAULT;

/**
 * 29.06.2021 - 16:28
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class AccountPostServiceImpl implements AccountPostService {

    private final PostRepository postRepository;

    @Autowired
    public AccountPostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public void accountPosts(Account owner, Model model) {
        Iterable<Post> posts = postRepository.findPostsByOwner(owner);
        model.addAttribute("posts", posts);
    }

    @Override
    public void accountNewPostAdd(Account owner, PostForm postForm) {
        Post post = Post.builder()
                .title(postForm.getTitle())
                .anons(postForm.getAnons())
                .fullText(postForm.getFullText())
                .views(VIEWS_DEFAULT)
                .owner(owner)
                .build();
        postRepository.save(post);
    }

    @Override
    public void accountPostDetails(Long id, Model model) {
        Optional<Post> post = postRepository.findById(id);
        ArrayList<Post> result = new ArrayList<>();
        post.ifPresent(result::add);
        model.addAttribute("post", result);
    }

    @Override
    public void accountPostEdit(Long id, Model model) {
        Optional<Post> post = postRepository.findById(id);
        ArrayList<Post> result = new ArrayList<>();
        post.ifPresent(result::add);
        model.addAttribute("post", result);
    }

    @Override
    public void accountPostUpdate(Long id, PostForm postForm, Model model) {
        Post post = postRepository.findById(id).orElseThrow();
        post.setTitle(postForm.getTitle());
        post.setAnons(postForm.getAnons());
        post.setFullText(postForm.getFullText());
        postRepository.save(post);
    }

    @Override
    public void accountPostRemove(Long id) {
        Post post = postRepository.findById(id).orElseThrow();
        postRepository.delete(post);
    }

    @Override
    public boolean isExistsPostInDb(Long id) {
        return !postRepository.existsById(id);
    }
}
