package ru.ptv.portfolio.services;

import org.springframework.ui.Model;
import ru.ptv.portfolio.forms.PageAdminAccountForm;

/**
 * 09.07.2021 - 17:05
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface PageAdminAccountsService {
    void findAllAccounts(Model model);
    void adminAccountEdit(Long id, Model model);
    void adminAccountUpdate(Long id, PageAdminAccountForm pageAdminAccountForm, Model model);
}
