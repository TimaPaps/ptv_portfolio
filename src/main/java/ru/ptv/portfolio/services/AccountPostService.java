package ru.ptv.portfolio.services;

import org.springframework.ui.Model;
import ru.ptv.portfolio.forms.PostForm;
import ru.ptv.portfolio.models.Account;


/**
 * 29.06.2021 - 16:22
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface AccountPostService {
    void accountPosts(Account owner, Model model);
    void accountNewPostAdd(Account owner, PostForm postForm);
    void accountPostDetails(Long id, Model model);
    void accountPostEdit(Long id, Model model);
    void accountPostUpdate(Long id, PostForm postForm, Model model);
    void accountPostRemove(Long id);
    boolean isExistsPostInDb(Long id);
}

