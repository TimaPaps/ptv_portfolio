package ru.ptv.portfolio.services;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import ru.ptv.portfolio.forms.PageAdminContactForm;
import ru.ptv.portfolio.models.PageContacts;
import ru.ptv.portfolio.repositories.PageContactsRepository;

import java.util.ArrayList;
import java.util.Optional;

/**
 * 08.07.2021 - 18:40
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class PageContactsServiceImpl implements PageContactsService {

    private final PageContactsRepository pageContactsRepository;

    public PageContactsServiceImpl(PageContactsRepository pageContactsRepository) {
        this.pageContactsRepository = pageContactsRepository;
    }

    @Override
    public void contactAdd(PageAdminContactForm pageAdminContactForm) {
        PageContacts pageContacts = PageContacts.builder()
                .image(pageAdminContactForm.getImage())
                .link(pageAdminContactForm.getLink())
                .alt(pageAdminContactForm.getAlt())
                .build();
        pageContactsRepository.save(pageContacts);
    }

    @Override
    public void findAllContacts(Model model) {
        Iterable<PageContacts> contacts = pageContactsRepository.findAll();
        model.addAttribute("pageContactsData", contacts);
    }

    @Override
    public void contactEdit(Long id, Model model) {
        Optional<PageContacts> pageContacts = pageContactsRepository.findById(id);
        ArrayList<PageContacts> result = new ArrayList<>();
        pageContacts.ifPresent(result::add);
        model.addAttribute("contact", result);
    }

    @Override
    public void contactUpdate(Long id, PageAdminContactForm pageAdminContactForm, Model model) {
        PageContacts pageContacts = pageContactsRepository.findById(id).orElseThrow();
        pageContacts.setImage(pageAdminContactForm.getImage());
        pageContacts.setLink(pageAdminContactForm.getLink());
        pageContacts.setAlt(pageAdminContactForm.getAlt());
        pageContactsRepository.save(pageContacts);
    }

    @Override
    public void contactRemove(Long id) {
        PageContacts pageContacts = pageContactsRepository.findById(id).orElseThrow();
        pageContactsRepository.delete(pageContacts);
    }

    @Override
    public boolean isExistsContactInDb(Long id) {
        return !pageContactsRepository.existsById(id);
    }
}
