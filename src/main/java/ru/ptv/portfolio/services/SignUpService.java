package ru.ptv.portfolio.services;

import ru.ptv.portfolio.forms.SignUpForm;

/**
 * 25.06.2021 - 15:04
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface SignUpService {
    void signUpAccountAdd(SignUpForm signUpForm);
}
