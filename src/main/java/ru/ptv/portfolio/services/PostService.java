package ru.ptv.portfolio.services;

import org.springframework.ui.Model;
import ru.ptv.portfolio.models.Post;

/**
 * 22.06.2021 - 22:59
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface PostService {
    void blogMain(Model model);
    void blogPostDetails(Long id, Model model);
    boolean isExistsPostInDb(Long id);
    void blogPostViewsUpdate(Post post);
}
