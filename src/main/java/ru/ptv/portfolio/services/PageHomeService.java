package ru.ptv.portfolio.services;

import org.springframework.ui.Model;
import ru.ptv.portfolio.forms.PageAdminHomeForm;

/**
 * 06.07.2021 - 13:33
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface PageHomeService {
    void pageHomeGet(Byte id, Model model);
    void pageHomeEdit(Byte id, Model model);
    void pageHomeUpdate(Byte id, PageAdminHomeForm pageAdminHomeForm, Model model);
}
