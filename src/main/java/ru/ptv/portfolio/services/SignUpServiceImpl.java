package ru.ptv.portfolio.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.ptv.portfolio.forms.SignUpForm;
import ru.ptv.portfolio.models.Account;
import ru.ptv.portfolio.models.Role;
import ru.ptv.portfolio.models.State;
import ru.ptv.portfolio.repositories.AccountRepository;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * 25.06.2021 - 15:11
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class SignUpServiceImpl implements SignUpService {

    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SignUpServiceImpl(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void signUpAccountAdd(SignUpForm signUpForm) {
        String hashPassword = passwordEncoder.encode(signUpForm.getPassword());

        Account account = Account.builder()
                .firstName(signUpForm.getFirstName())
                .lastName(signUpForm.getLastName())
                .email(signUpForm.getEmail())
                .phone(signUpForm.getPhone())
                .hashPassword(hashPassword)
                .dateRegistration(Timestamp.valueOf(LocalDateTime.now()))
                .role(Role.USER)
                .state(State.ACTIVE)
                .build();

        accountRepository.save(account);
    }
}
