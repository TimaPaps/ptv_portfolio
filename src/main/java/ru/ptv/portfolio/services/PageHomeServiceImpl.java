package ru.ptv.portfolio.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import ru.ptv.portfolio.forms.PageAdminHomeForm;
import ru.ptv.portfolio.models.PageHome;
import ru.ptv.portfolio.repositories.PageHomeRepository;

import java.util.ArrayList;
import java.util.Optional;

/**
 * 06.07.2021 - 14:13
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class PageHomeServiceImpl implements PageHomeService {

    private final PageHomeRepository pageHomeRepository;

    @Autowired
    public PageHomeServiceImpl(PageHomeRepository pageHomeRepository) {
        this.pageHomeRepository = pageHomeRepository;
    }

    @Override
    public void pageHomeGet(Byte id, Model model) {
        Optional<PageHome> pageHome = pageHomeRepository.findById(id);
        ArrayList<PageHome> result = new ArrayList<>();
        pageHome.ifPresent(result::add);
        model.addAttribute("pageHomeData", result);
    }

    @Override
    public void pageHomeEdit(Byte id, Model model) {
        Optional<PageHome> pageHome = pageHomeRepository.findById(id);
        ArrayList<PageHome> result = new ArrayList<>();
        pageHome.ifPresent(result::add);
        model.addAttribute("pageHomeData", result);
    }

    @Override
    public void pageHomeUpdate(Byte id, PageAdminHomeForm pageAdminHomeForm, Model model) {
        PageHome pageHome = pageHomeRepository.findById(id).orElseThrow();
        pageHome.setText_header(pageAdminHomeForm.getText_header());
        pageHome.setText_intro(pageAdminHomeForm.getText_intro());
        pageHome.setText_desc(pageAdminHomeForm.getText_desc());
        pageHomeRepository.save(pageHome);
    }
}
