package ru.ptv.portfolio.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import ru.ptv.portfolio.models.Post;
import ru.ptv.portfolio.repositories.PostRepository;

import java.util.ArrayList;
import java.util.Optional;

import static ru.ptv.portfolio.consts.Constants.VIEWS_DEFAULT;

/**
 * 22.06.2021 - 23:23
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public void blogMain(Model model) {
        Iterable<Post> posts = postRepository.findAll();
        model.addAttribute("posts", posts);
    }

    @Override
    public void blogPostDetails(Long id, Model model) {
        Optional<Post> post = postRepository.findById(id);

        if (post.isPresent()) {
            blogPostViewsUpdate(post.get());
        } else {
            throw new IllegalArgumentException("Post NotFound");
        }
        ArrayList<Post> result = new ArrayList<>();
        post.ifPresent(result::add);
        model.addAttribute("post", result);
    }

    @Override
    public void blogPostViewsUpdate(Post post) {
        if (post.getViews() == null) {
            post.setViews(VIEWS_DEFAULT);
            postRepository.save(post);
        }
        post.setViews(post.getViews() + 1);
        postRepository.save(post);
    }

    @Override
    public boolean isExistsPostInDb(Long id) {
        return !postRepository.existsById(id);
    }
}
