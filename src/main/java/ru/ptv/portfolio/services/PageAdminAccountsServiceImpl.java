package ru.ptv.portfolio.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import ru.ptv.portfolio.forms.PageAdminAccountForm;
import ru.ptv.portfolio.models.Account;
import ru.ptv.portfolio.repositories.AccountRepository;

import java.util.ArrayList;
import java.util.Optional;

/**
 * 09.07.2021 - 17:06
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class PageAdminAccountsServiceImpl implements PageAdminAccountsService {

    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public PageAdminAccountsServiceImpl(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void findAllAccounts(Model model) {
        Iterable<Account> accounts = accountRepository.findAll();
        model.addAttribute("pageAdminAccountsData", accounts);
    }

    @Override
    public void adminAccountEdit(Long id, Model model) {
        Optional<Account> account = accountRepository.findById(id);
        ArrayList<Account> result = new ArrayList<>();
        account.ifPresent(result::add);
        model.addAttribute("account", result);
    }

    @Override
    public void adminAccountUpdate(Long id, PageAdminAccountForm pageAdminAccountForm, Model model) {
        Account account = accountRepository.findById(id).orElseThrow();
        account.setFirstName(pageAdminAccountForm.getFirstName());
        account.setLastName(pageAdminAccountForm.getLastName());
        account.setEmail(pageAdminAccountForm.getEmail());
        account.setPhone(pageAdminAccountForm.getPhone());
        account.setRole(pageAdminAccountForm.getRole());
        account.setState(pageAdminAccountForm.getState());
        accountRepository.save(account);
    }
}
