package ru.ptv.portfolio.services;

import org.springframework.ui.Model;
import ru.ptv.portfolio.forms.PageAdminContactForm;

/**
 * 08.07.2021 - 18:35
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface PageContactsService {
    void contactAdd(PageAdminContactForm pageAdminContactForm);
    void findAllContacts(Model model);
    void contactEdit(Long id, Model model);
    void contactUpdate(Long id, PageAdminContactForm pageAdminContactForm, Model model);
    void contactRemove(Long id);
    boolean isExistsContactInDb(Long id);
}
