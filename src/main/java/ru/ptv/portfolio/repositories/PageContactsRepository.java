package ru.ptv.portfolio.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.ptv.portfolio.models.PageContacts;

/**
 * 08.07.2021 - 18:31
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface PageContactsRepository extends CrudRepository<PageContacts, Long> {

}
