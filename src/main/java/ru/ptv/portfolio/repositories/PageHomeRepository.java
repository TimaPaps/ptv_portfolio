package ru.ptv.portfolio.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.ptv.portfolio.models.PageHome;

/**
 * 06.07.2021 - 12:41
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface PageHomeRepository extends CrudRepository<PageHome, Byte> {
}
