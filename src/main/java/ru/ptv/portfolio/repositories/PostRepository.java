package ru.ptv.portfolio.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.ptv.portfolio.models.Account;
import ru.ptv.portfolio.models.Post;

/**
 * 21.06.2021 - 21:42
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface PostRepository extends CrudRepository<Post, Long> {
    Iterable<Post> findPostsByOwner(Account owner);
}
