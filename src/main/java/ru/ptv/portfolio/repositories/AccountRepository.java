package ru.ptv.portfolio.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.ptv.portfolio.models.Account;

import java.util.Optional;

/**
 * 24.06.2021 - 17:40
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface AccountRepository extends CrudRepository<Account, Long> {
    Optional<Account> findOneByEmail(String email);
}
