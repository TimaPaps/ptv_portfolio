package ru.ptv.portfolio.security.details;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.ptv.portfolio.repositories.AccountRepository;

/**
 * 26.06.2021 - 22:46
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class AccountDetailsServiceImpl implements UserDetailsService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountDetailsServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return new AccountDetailsImpl(accountRepository.findOneByEmail(email)
                .orElseThrow(IllegalArgumentException::new));
    }
}
