package ru.ptv.portfolio.utils;

import org.springframework.security.core.Authentication;
import ru.ptv.portfolio.dto.AccountDto;
import ru.ptv.portfolio.security.details.AccountDetailsImpl;

/**
 * 29.06.2021 - 0:15
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Util {
    public static AccountDto createAccountDto(Authentication authentication) {
        AccountDetailsImpl accountDetails = (AccountDetailsImpl) authentication.getPrincipal();
        return AccountDto.from(accountDetails.getAccount());
    }
}
