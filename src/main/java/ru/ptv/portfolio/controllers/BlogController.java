package ru.ptv.portfolio.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.ptv.portfolio.services.PostService;

/**
 * 21.06.2021 - 17:37
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Controller
public class BlogController {

    private final PostService postService;

    @Autowired
    public BlogController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/blog")
    public String blogMain(Authentication authentication, Model model) {
        if (authentication != null) {
            return "redirect:/accountBlog";
        }
        postService.blogMain(model);
        return "main/blogMain";
    }

    @GetMapping("/blog/{id}")
    public String blogPostDetails(Authentication authentication, @PathVariable(value = "id") Long id, Model model) {
        if (authentication != null) {
            return "redirect:/accountPostDetails/{id}";
        }

        if(postService.isExistsPostInDb(id)) {
            return "redirect:/blog";
        }
        postService.blogPostDetails(id, model);
        return "main/blogPostDetails";
    }
}
