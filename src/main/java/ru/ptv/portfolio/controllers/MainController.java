package ru.ptv.portfolio.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.ptv.portfolio.services.PageContactsService;
import ru.ptv.portfolio.services.PageHomeService;

import static ru.ptv.portfolio.consts.Constants.ONLY_ONE_ENTRY;

/**
 * 21.06.2021 - 16:11
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Controller
public class MainController {

    private final PageHomeService pageHomeService;
    private final PageContactsService pageContactsService;

    @Autowired
    public MainController(PageHomeService pageHomeService, PageContactsService pageContactsService) {
        this.pageHomeService = pageHomeService;
        this.pageContactsService = pageContactsService;
    }

    @GetMapping("/")
    public String home(Model model) {
        pageHomeService.pageHomeGet(ONLY_ONE_ENTRY, model);
        return "index";
    }

    @GetMapping("/photo")
    public String photo(Model model) {
        model.addAttribute("title", "Страница Photo");
        return "main/photo";
    }

    @GetMapping("/video")
    public String video(Model model) {
        model.addAttribute("title", "Страница Video");
        return "main/video";
    }

    @GetMapping("/about")
    public String about(Model model) {
        model.addAttribute("title", "Страница About");
        return "main/about";
    }

    @GetMapping("/contacts")
    public String contacts(Model model) {
        pageContactsService.findAllContacts(model);
        return "main/contacts";
    }
}
