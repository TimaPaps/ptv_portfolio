package ru.ptv.portfolio.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.ptv.portfolio.dto.AccountDto;
import ru.ptv.portfolio.forms.PostForm;
import ru.ptv.portfolio.models.Account;
import ru.ptv.portfolio.security.details.AccountDetailsImpl;
import ru.ptv.portfolio.services.AccountPostService;
import ru.ptv.portfolio.utils.Util;

/**
 * 29.06.2021 - 15:53
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Controller
public class AccountController {

    private final AccountPostService accountPostService;

    @Autowired
    public AccountController(AccountPostService accountPostService) {
        this.accountPostService = accountPostService;
    }

    @GetMapping("/accountBlog")
    public String accountBlog(Authentication authentication, Model model) {
        AccountDetailsImpl accountDetails = (AccountDetailsImpl) authentication.getPrincipal();
        Account owner = accountDetails.getAccount();
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        accountPostService.accountPosts(owner, model);
        return "account/index";
    }

    @GetMapping("/accountPost/add")
    public String accountPostAdd(Authentication authentication, Model model) {
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        return "account/accountPostAdd";
    }

    @PostMapping("/accountPost/add")
    public String accountNewPostAdd(Authentication authentication, PostForm postForm) {
        AccountDetailsImpl accountDetails = (AccountDetailsImpl) authentication.getPrincipal();
        Account owner = accountDetails.getAccount();
        accountPostService.accountNewPostAdd(owner, postForm);
        return "redirect:/accountBlog";
    }

    @GetMapping("/accountPostDetails/{id}")
    public String accountPostDetails(Authentication authentication, @PathVariable(value = "id") Long id, Model model) {
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        accountPostService.accountPostDetails(id, model);
        return "account/accountPostDetails";
    }

    @GetMapping("/accountPost/{id}/edit")
    public String accountPostEdit(Authentication authentication, @PathVariable(value = "id") Long id, Model model) {
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        if(accountPostService.isExistsPostInDb(id)) {
            return "redirect:/accountBlog";
        }
        accountPostService.accountPostEdit(id, model);
        return "account/accountPostEdit";
    }

    @PostMapping("/accountPost/{id}/edit")
    public String accountPostUpdate(@PathVariable(value = "id") Long id, PostForm postForm, Model model) {
        accountPostService.accountPostUpdate(id, postForm, model);
        return "redirect:/accountBlog";
    }
    
    @PostMapping("/accountPost/{id}/remove")
    public String accountPostRemove(@PathVariable(value = "id") Long id) {
        accountPostService.accountPostRemove(id);
        return "redirect:/accountBlog";
    }
}
