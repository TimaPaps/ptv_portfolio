package ru.ptv.portfolio.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.ptv.portfolio.dto.AccountDto;
import ru.ptv.portfolio.forms.PageAdminAccountForm;
import ru.ptv.portfolio.forms.PageAdminContactForm;
import ru.ptv.portfolio.forms.PageAdminHomeForm;
import ru.ptv.portfolio.services.PageAdminAccountsService;
import ru.ptv.portfolio.services.PageContactsService;
import ru.ptv.portfolio.services.PageHomeService;
import ru.ptv.portfolio.utils.Util;

import static ru.ptv.portfolio.consts.Constants.ONLY_ONE_ENTRY;

/**
 * 03.07.2021 - 23:50
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Controller
public class AdminController {

    private final PageHomeService pageHomeService;
    private final PageContactsService pageContactsService;
    private final PageAdminAccountsService pageAdminAccountsService;

    @Autowired
    public AdminController(PageHomeService pageHomeService,
                           PageContactsService pageContactsService,
                           PageAdminAccountsService pageAdminAccountsService) {
        this.pageHomeService = pageHomeService;
        this.pageContactsService = pageContactsService;
        this.pageAdminAccountsService = pageAdminAccountsService;
    }

    @GetMapping("/admin")
    public String adminPage(Authentication authentication, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        return "admin/index";
    }

    @GetMapping("/admin/home")
    public String adminHome(Authentication authentication, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        pageHomeService.pageHomeEdit(ONLY_ONE_ENTRY, model);
        return "admin/adminHome";
    }

    @PostMapping("/admin/home")
    public String pageHomeUpdate(Authentication authentication, PageAdminHomeForm pageAdminHomeForm, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        pageHomeService.pageHomeUpdate(ONLY_ONE_ENTRY, pageAdminHomeForm, model);
        pageHomeService.pageHomeEdit(ONLY_ONE_ENTRY, model);
        return "admin/adminHome";
    }

    @GetMapping("/admin/photo")
    public String adminPhoto(Authentication authentication, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        return "admin/adminPhoto";
    }

    @GetMapping("/admin/video")
    public String adminVideo(Authentication authentication, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        return "admin/adminVideo";
    }

    @GetMapping("/admin/about")
    public String adminAbout(Authentication authentication, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        return "admin/adminAbout";
    }

    @GetMapping("/admin/contacts")
    public String adminContacts(Authentication authentication, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        pageContactsService.findAllContacts(model);
        return "admin/adminContacts";
    }

    @PostMapping("/admin/contacts")
    public String adminContactAdd(Authentication authentication,
                                  PageAdminContactForm pageAdminContactForm,
                                  Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        pageContactsService.contactAdd(pageAdminContactForm);
        pageContactsService.findAllContacts(model);
        return "admin/adminContacts";
    }

    @GetMapping("/admin/contacts/{id}/edit")
    public String adminContactEdit(Authentication authentication, @PathVariable(value = "id") Long id, Model model)  {
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        if (pageContactsService.isExistsContactInDb(id)) {
            return "redirect:/admin/adminContacts";
        }
        pageContactsService.contactEdit(id, model);
        return "admin/adminContactEdit";
    }

    @PostMapping("/admin/contacts/{id}/edit")
    public String adminContactUpdate(Authentication authentication,
                                     @PathVariable(value = "id") Long id,
                                     PageAdminContactForm pageAdminContactForm,
                                     Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        pageContactsService.contactUpdate(id, pageAdminContactForm, model);
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        pageContactsService.findAllContacts(model);
        return "admin/adminContacts";
    }

    @PostMapping("/admin/contacts/{id}/remove")
    public String adminContactRemove(Authentication authentication, @PathVariable(value = "id") Long id, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        pageContactsService.contactRemove(id);
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        pageContactsService.findAllContacts(model);
        return "admin/adminContacts";
    }

    @GetMapping("/admin/accounts")
    public String adminAccounts(Authentication authentication, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        pageAdminAccountsService.findAllAccounts(model);
        return "admin/adminAccounts";
    }

    @GetMapping("/admin/accounts/{id}/edit")
    public String adminAccountEdit(Authentication authentication, @PathVariable(value = "id") Long id, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        pageAdminAccountsService.adminAccountEdit(id, model);
        return "admin/adminAccountEdit";
    }

    @PostMapping("/admin/accounts/{id}/edit")
    public String adminAccountUpdate(Authentication authentication,
                                     @PathVariable(value = "id") Long id,
                                     PageAdminAccountForm pageAdminAccountForm,
                                     Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        pageAdminAccountsService.adminAccountUpdate(id, pageAdminAccountForm, model);
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        pageAdminAccountsService.findAllAccounts(model);
        return "admin/adminAccounts";
    }

    @GetMapping("/admin/posts")
    public String adminPosts(Authentication authentication, Model model) {
        if (isAuthentication(authentication)) return "redirect:/signIn";
        AccountDto accountDto = Util.createAccountDto(authentication);
        model.addAttribute(accountDto);
        return "admin/adminPosts";
    }

    private boolean isAuthentication(Authentication authentication) {
        return authentication == null;
    }
}
