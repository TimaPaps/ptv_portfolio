package ru.ptv.portfolio.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.ptv.portfolio.forms.SignUpForm;
import ru.ptv.portfolio.services.SignUpService;

/**
 * 24.06.2021 - 19:54
 * PTV_Portfolio
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Controller
public class SignUpController {

    private final SignUpService signUpService;

    @Autowired
    public SignUpController(SignUpService signUpService) {
        this.signUpService = signUpService;
    }

    @GetMapping("/signUp")
    public String signUp(Model model) {
        return "main/signUp";
    }

    @PostMapping("/signUp")
    public String signUpAccountAdd(SignUpForm signUpForm, Model model) {
            signUpService.signUpAccountAdd(signUpForm);
        return "redirect:/signIn";
    }
}
